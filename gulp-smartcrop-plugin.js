const through = require('through2')
const gm = require('gm').subClass({ imageMagick: true })
const smartcrop = require('smartcrop-gm')
const fs = require('fs')
const Vinyl = require('vinyl')
const path = require('path')
const BUFFER_FORMAT = 'JPG'

function applySmartCrop(file, width, height) {
    let start = new Date().getTime()
    return new Promise((resolve, reject) => {
        
        var imageAsJpg = gm(file.path)
            .resize(1024, 1024)
            .flatten() // fundo branco / png -> jpg
            .toBuffer(BUFFER_FORMAT, (err, bufferWithWhite) => {
                gm(bufferWithWhite)
                    .gravity('Center') // atrai o conteudo para o centro
                    .extent(1024, 1024) // aumenta o canvas
                    .toBuffer(BUFFER_FORMAT, (err, extendedImg) => {
                        smartcrop.crop(extendedImg, { width, height })
                            .then((result) => {
                                var crop = result.topCrop
                                    gm(extendedImg)
                                    .crop(crop.width, crop.height, crop.x, crop.y)
                                    .resize(width, height)
                                    .toBuffer(BUFFER_FORMAT,function (err, buffer) {
                                        let finalFile = new Vinyl({...file, contents: buffer})
                                        let end = new Date().getTime() - start
                                        console.log("processed: ", finalFile.relative, "\t Took: "  , end , "ms", " \tResult: ", err ? err : " [OK]")
                                        if (err) return reject(err)
                                        resolve(finalFile)
                                    })
                            })
                            .catch(reject)
                    })
            })
    })
}


module.exports = function({width, height}) {
    var pushResult = (transformation, file, callback) => {
        transformation.push(file)
        callback()
    }
    
    var transform = function(file, encoding, callback) {
        let transformation = this
        applySmartCrop(file, width, height, transformation)
            .then((newFile) => {
                pushResult(transformation, newFile, callback)
            })
            .catch((err) => {
                console.error(err)
                callback()
            })
    }
  
    return through.obj(transform)
}
