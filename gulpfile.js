const gulp = require('gulp')
const imagemin = require('gulp-imagemin')
const smartCrop = require('./gulp-smartcrop-plugin')
const path = require('path')

const source = process.cwd() + '/images/*'
const dest = process.cwd() + '/dist/'

console.log(source, '\n' , dest)

gulp.task('default', () =>
	gulp.src(source)
        .pipe(smartCrop({
            width: 400,
            height: 400
        }))
        .pipe(imagemin([
            imagemin.jpegtran()
        ]))
		.pipe(gulp.dest(dest))
)
